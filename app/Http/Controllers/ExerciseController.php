<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ExerciseController extends Controller
{
    public function index(): View
    {
        $customers = Customer::get();
        return view('exercise.index', [
            'customers' => $customers
        ]);
    }

    public function display(Request $request): View
    {

        $email = $request->input('email');
        
        $customer = Customer::where('email', $email)->firstOrFail();

        return view('exercise.display', [
            'customer' => $customer
        ]);
    }

    public function create(): View
    {
        return view('exercise.create');
    }

    public function insert(Request $request)
    {
        // Validate the request
        $validator = Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|email|unique:customers,email',
            'city' => 'required|string|max:255',
            'country' => ['required','string','max:255', Rule::in(collect(config('constants.available_countries'))->pluck('code')->all())],
            'picture' => 'required|image|max:2048',
        ]);

        if($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput($request->all());
        }

        $customer = new Customer();
        $customer->firstname = $request->get('firstname');
        $customer->lastname = $request->get('lastname');
        $customer->email = $request->get('email');
        $customer->city = $request->get('city');
        $customer->country = $request->get('country');

        if ($image = $request->file('picture'))
        {
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $test = Storage::putFileAs('public/images', $image, $filename);
            $path = Storage::url('images/'.$filename);
            $customer->picture = $path;
        }
        

        $customer->save();

        return redirect()->route('exercise.display', ['email' => $customer->email])->with('message', 'Customer created successfully!');
    }

    public function edit($id): View
    {
        
        $customer = Customer::where('id', $id)->firstOrFail();

        return view('exercise.edit', [
            'customer' => $customer
        ]);
    }

    public function update(Request $request, $id)
    {
        $customer = Customer::where('id', $id)->firstOrFail();
        
        // Validate the request
        $validator = Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|email|unique:customers,email,'.$customer->id,
            'city' => 'required|string|max:255',
            'country' => ['required','string','max:255', Rule::in(collect(config('constants.available_countries'))->pluck('code')->all())]
        ]);

        if($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput($request->all());
        }

        $customer->firstname = $request->get('firstname');
        $customer->lastname = $request->get('lastname');
        $customer->email = $request->get('email');
        $customer->city = $request->get('city');
        $customer->country = $request->get('country');

        $customer->save();
    

        return redirect()->route('exercise.display', ['email' => $customer->email])->with('message', 'Customer updated successfully!');
        
    }

    public function update_image(Request $request, $id)
    {
        $customer = Customer::where('id', $id)->firstOrFail();

        $request->validate([
            'picture' => 'required|image|max:2048',
        ]);

        $image = $request->file('picture');
        $filename = time() . '.' . $image->getClientOriginalExtension();
        $test = Storage::putFileAs('public/images', $image, $filename);
        // $result = $image->storeAs('public/images', $filename);
        $path = Storage::url('images/'.$filename);

        // Storage::delete($user->image); // Delete the old image
        $customer->picture = $path;

        $customer->save();

        return response()->json(['picture_url' => $path,'message' => "Image successfully updated.", 'result' => $test, 'filename' => $filename]);

        
    }

}
