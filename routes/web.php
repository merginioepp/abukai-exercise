<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\ExerciseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ExerciseController::class, 'index'])->name('exercise.index');
Route::get('/display', [ExerciseController::class, 'display'])->name('exercise.display');
Route::get('/create', [ExerciseController::class, 'create'])->name('exercise.create');
Route::post('/create', [ExerciseController::class, 'insert'])->name('exercise.insert');
Route::patch('/edit/{id}', [ExerciseController::class, 'update'])->name('exercise.update');
Route::get('/edit/{id}', [ExerciseController::class, 'edit'])->name('exercise.edit');


Route::get('calculator/keys', function(){
    return view('pocketcalc.calckeys');
})->name('calc.keys');

Route::get('calculator/display', function(){
    return view('pocketcalc.calcdisplay');
})->name('calc.display');
