<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    @vite(['resources/scss/app.scss', 'resources/js/app.js'])

    @hasSection('additionalcss')
        @yield('additionalcss')
    @endif
</head>
<body>
    <div class="container">
        @yield('content')
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    @hasSection('additionaljs')
        @yield('additionaljs')
    @endif

    @hasSection('scripts')
        @yield('scripts')
    @endif
</body>
</html>