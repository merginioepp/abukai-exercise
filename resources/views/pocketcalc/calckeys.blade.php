<!DOCTYPE html>
<html>
<head>
	<title>Calculator Buttons</title>
	<meta charset="UTF-8">
	<style>
		button {
			height: 50px;
			font-size: 20px;
		}

        body {
            display: grid;
            grid-template-columns: 1fr 1fr 1fr;
        }
	</style>
</head>
<body>
	<button onclick="buttonPressed('7')">7</button>
	<button onclick="buttonPressed('8')">8</button>
	<button onclick="buttonPressed('9')">9</button>
	<button onclick="buttonPressed('4')">4</button>
	<button onclick="buttonPressed('5')">5</button>
	<button onclick="buttonPressed('6')">6</button>
	<button onclick="buttonPressed('1')">1</button>
	<button onclick="buttonPressed('2')">2</button>
	<button onclick="buttonPressed('3')">3</button>
	<button onclick="buttonPressed('0')">0</button>
	<button onclick="buttonPressed('+')">+</button>
	<button onclick="buttonPressed('-')">-</button>
	<button onclick="buttonPressed('=')">=</button>

    <script>
        function buttonPressed(val) {
            parent.postMessage(val, window.location.origin);
        }
    </script>
</body>
</html>