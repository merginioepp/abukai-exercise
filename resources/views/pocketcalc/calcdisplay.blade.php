<!DOCTYPE html>
<html>
<head>
	<title>Calculator Display</title>
	<meta charset="UTF-8">
	<style>
		body {
			margin: 0;
			padding: 0;
			font-size: 20px;
			text-align: right;
			background-color: #eee;
		}
	</style>
</head>
<body>
	<span id="display"></span>
    <script>
        window.addEventListener('message', function(event) {
            //security
            if (event.origin !== window.location.origin) return;

            var display = document.querySelector("#display");
            if (event.data == "=") {
                window.parent.postMessage(display.innerHTML, window.location.origin);
                display.innerHTML = "";
            }
            else {
                display.innerHTML += event.data;
            }
        });
    </script>
</body>
</html>