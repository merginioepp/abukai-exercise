@extends('shared.layoutbase')

@section('additionalcss')
<style>
   label.error {color: red }
</style>
@endsection

@section('content')
<h1>Edit Customer Information</h1>
<hr>
<form action="{{ route('exercise.update', ['id' => $customer->id]) }}" id="editForm" method="post">
   @method('PATCH')
   @csrf
   <div class="mb-3">
      <label for="lastname" class="form-label">Last Name</label>
      <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name" value="{{ old('lastname',$customer->lastname) }}">
   </div>
   <div class="mb-3">
      <label for="firstname" class="form-label">First Name</label>
      <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name" value="{{ old('firstname',$customer->firstname) }}">
   </div>
   <div class="mb-3">
      <label for="email" class="form-label">Email Address</label>
      <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="{{ old('email',$customer->email) }}">
   </div>
   <div class="mb-3">
      <label for="city" class="form-label">City</label>
      <input type="text" class="form-control" id="city" name="city" placeholder="City" value="{{ old('city',$customer->city) }}">
   </div>
   <div class="mb-3">
      <label for="country" class="form-label">Country</label>
      <select class="form-select" id="country" name="country" aria-label="Select Country">
         <option value="">Select a country</option>

         @foreach (config('constants.available_countries') as $country)
         <script>console.log("{{ $country['code'] }}", "{{ $customer->country }}")</script>
        <option value="{{ $country['code'] }}" {{ ($country["code"] == old('country', $customer->country)) ? 'selected' : '' }} >
            {{ $country['description'] }}
        </option>
         @endforeach
      </select>
   </div>
   <div class="mb-3">
      <label for="picture" class="form-label">Upload Picture</label>
      <label for="picture" class="btn btn-primary">Upload</label>
      <input class="form-control" type="file" accept=".jpg" id="picture" style="display: none;">
         
      
      
   </div>
   <div class="mb-3">
      <label for="picture-preview" class="form-label">Picture Preview</label>
      <img id="picture-preview" src="{{ $customer->picture }}" height="300" width="300" alt="picture-preview">
   </div>
   <hr>
   <button type="submit" class="btn btn-primary">Submit</button>
   <a href="{{ url()->previous() }}" class="btn btn-danger">Back</a>

</form>
@endsection

@section('additionaljs')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
@endsection

@section('scripts')
<script>
   $(document).ready(function() {
      var available_countries = @json(collect(config('constants.available_countries'))->pluck('code')->all(), JSON_PRETTY_PRINT);
      $.validator.addMethod("availablecountries", function(value, element) {
         return available_countries.indexOf(value) != -1;
      }, "Must be in available countries");

      $('#editForm').validate(
         {
            rules: {
               lastname: { required: true, maxlength: 255 },
               firstname: { required: true, maxlength: 255 },
               email: { required: true, maxlength: 255, email: true },
               city: { required: true, maxlength: 255 },
               country: { required: true, availablecountries: true }
            }
         }
      );
      $('#picture').on('change', function() {
         let imagePreview = $('#picture-preview');
         let file = this.files[0];
         
         let formData = new FormData();
         formData.append('picture', this.files[0]);
         formData.append('_method', 'PATCH');

         $.ajax({
            url: "{{ route('exercise.update_image', ['id' => $customer->id]) }}",
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: function(data) {

               //load local reference of the image in the image-preview
               let reader = new FileReader();
               reader.onload = function() {
                  imagePreview.attr('src', reader.result);
               }
               reader.readAsDataURL(file);
            },
            error: function(xhr, status, error) {
               console.log(status, error);
               alert('Something went wrong');
            }
         });

         
      });
   }); 
</script>
   
@endsection