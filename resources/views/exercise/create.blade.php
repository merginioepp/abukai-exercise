@extends('shared.layoutbase')

@section('additionalcss')
<style>
   label.error {color: red }
</style>
@endsection

@section('content')
<h1>Create Customer Information</h1>
<hr>
<form action="{{ route('exercise.create') }}" id="editForm" method="post" enctype="multipart/form-data">
   @method('POST')
   @csrf
   <div class="mb-3">
      <label for="lastname" class="form-label">Last Name</label>
      <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name" value="">
   </div>
   <div class="mb-3">
      <label for="firstname" class="form-label">First Name</label>
      <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name" value="">
   </div>
   <div class="mb-3">
      <label for="email" class="form-label">Email Address</label>
      <input type="email" class="form-control" id="email" name="email" placeholder="Email Address" value="">
   </div>
   <div class="mb-3">
      <label for="city" class="form-label">City</label>
      <input type="text" class="form-control" id="city" name="city" placeholder="City" value="">
   </div>
   <div class="mb-3">
      <label for="country" class="form-label">Country</label>
      <select class="form-select" id="country" name="country" aria-label="Select Country">
         <option value="" selected>Select a country</option>

         @foreach (config('constants.available_countries') as $country)
        <option value="{{ $country['code'] }}">
            {{ $country['description'] }}
        </option>
         @endforeach
      </select>
   </div>
   <div class="mb-3">
      <label for="picture" class="form-label">Upload Picture</label>
      <input type="file" name ="picture" class="form-control" accept="image/jpeg" id="picture">
         
      
      
   </div>
   <div class="mb-3">
      <label for="picture-preview" class="form-label">Picture Preview</label>
      <img id="picture-preview" src="{{ old('picture', '/storage/images/default.jpg') }}" height="300" width="300" alt="picture-preview">
   </div>
   <hr>
   <button type="submit" class="btn btn-primary">Submit</button>
   <a href="{{ url()->previous() }}" class="btn btn-danger">Back</a>

</form>
@endsection

@section('additionaljs')
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
@endsection

@section('scripts')
<script>
   $(document).ready(function() {
      var available_countries = @json(collect(config('constants.available_countries'))->pluck('code')->all(), JSON_PRETTY_PRINT);
      $.validator.addMethod("availablecountries", function(value, element) {
         return available_countries.indexOf(value) != -1;
      }, "Must be in available countries");

      $('#editForm').validate(
         {
            rules: {
               lastname: { required: true, maxlength: 255 },
               firstname: { required: true, maxlength: 255 },
               email: { required: true, maxlength: 255, email: true },
               city: { required: true, maxlength: 255 },
               country: { required: true, availablecountries: true },
               picture: { required: true }
            }
         }
      );
      $('#picture').on('change', function() {
         let imagePreview = $('#picture-preview');
         let file = this.files[0];
         
         //load local reference of the image in the image-preview
         let reader = new FileReader();
        reader.onload = function() {
            imagePreview.attr('src', reader.result);
        }
        reader.readAsDataURL(file);

         
      });
   }); 
</script>
   
@endsection