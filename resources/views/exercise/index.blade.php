@extends('shared.layoutbase')

@section('content')
<h1>Customer List</h1>
<a href="{{ route('exercise.create') }}" class="btn btn-primary">Create Customer</a>
<hr>
<div class="">
    @foreach($customers as $customer)
    <div class="d-flex align-items-center mb-2">
        <a href="{{ route('exercise.display', ['email' => $customer->email]) }}" class="me-2"><img src="{{ $customer['picture'] }}" width="75" height="75" class="rounded-circle" alt="{{ $customer['email'] }}"></a>
            
        <a href="{{ route('exercise.display', ['email' => $customer->email]) }}"><h4>{{ $customer['email'] }}</h4></a>

        <a href="{{ route('exercise.display', ['email' => $customer->email]) }}" class="ms-auto me-3"><h4>View</h4></a>
        <a href="{{ route('exercise.edit', ['id' => $customer->id]) }}" class=""><h4>Edit</h4></a>
        <!-- <pre>{{ $customer }}</pre> -->
    </div>
    @endforeach
</div>
@endsection