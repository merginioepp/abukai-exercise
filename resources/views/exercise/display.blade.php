@extends('shared.layoutbase')

@section('content')
<h1>View Customer Information</h1>
<div class="d-flex">
    <div class="p-2 flex-grow-1"><img src="{{ $customer->picture }}" class="rounded-circle mb-5" height="200" width="200" alt="{{ $customer->email }}"></div>
    <div class="p-2 me-auto"><a href="{{ route('exercise.edit', ['id' => $customer->id]) }}" class="btn btn-primary">Edit</a></div>
    <div class="p-2 me-auto"><a href="{{ route('exercise.index') }}" class="btn btn-primary">Go to List</a></div>
</div>

<div class="d-flex">
    <div class="p-2 flex-fill">Last Name: <strong>{{ $customer->lastname }}</strong></div>
    <div class="p-2 flex-fill">First Name: <strong>{{ $customer->firstname }}</strong></div>
</div>
<div class="d-flex">
    <div class="p-2 flex-fill">Email : <strong>{{ $customer->email }}</strong></div>
</div>
<div class="d-flex">
    <div class="p-2 flex-fill">City: <strong>{{ $customer->city }}</strong></div>
    <div class="p-2 flex-fill">Country: <strong>{{ collect(config('constants.available_countries'))->firstWhere('code', $customer->country)['description'] }}</strong></div>
</div>

<hr>

<div>
    Mini-Calculator
	<div class="mt-3">
        <label for="result">Result:</label>
        <input id="result" class="mb-3" type="text" readonly>
    </div>
    <div>
        <iframe id="calc-display" src="{{ route('calc.display') }}"></iframe>
    </div>
	<div>
        <iframe id="calc-buttons" src="{{ route('calc.keys') }}" height="300"></iframe>
    </div>
    
</div>
@endsection

@section('scripts')
<script>
    var keyFrame = document.getElementById('calc-buttons');
    var displayFrame = document.getElementById('calc-display');
    var resultBox = document.getElementById('result');

    window.addEventListener('message', function(event) {
        if (event.origin !== window.location.origin) return;
        console.log("Message received from the child: " + event.data); // Message received from child

        //sender is keys. forward to display
        if(event.source == keyFrame.contentWindow) {
            displayFrame.contentWindow.postMessage(event.data, window.origin);
        }

        //sender is display. expecting equal, compute result
        if(event.source == displayFrame.contentWindow) {
            var result = eval(event.data);
            console.log("RESULT", result);
            resultBox.value = result;
        }
    });
</script>
@endsection