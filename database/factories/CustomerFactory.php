<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use App\Models\Customer;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Customer>
 */
class CustomerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

     protected $model = Customer::class;

    public function definition()
    {
        return [
            'firstname' => fake()->firstName(),
            'lastname' => fake()->lastName(),
            'email' => fake()->safeEmail(),
            'city' => fake()->city(),
            'country' => fake()->randomElement(collect(config('constants.available_countries'))->pluck('code')->all()),
            'picture' => fake()->randomElement([
                Storage::url('images/714.jpg'),
                Storage::url('images/717.jpg'),
                Storage::url('images/4667.jpg'),
                Storage::url('images/5435.jpg'),
                Storage::url('images/6788.jpg'),
                Storage::url('images/7455.jpg'),
                Storage::url('images/9556.jpg'),
                Storage::url('images/23454.jpg'),
                Storage::url('images/25913.jpg'),
                Storage::url('images/898758.jpg'),
            ])
        ];
    }
}
