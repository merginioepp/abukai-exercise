<?php

return [
    'available_countries' => [
        ['code' => 'us', 'description' => 'United States'],
        ['code' => 'ca', 'description' => 'Canada'],
        ['code' => 'jp', 'description' => 'Japan'],
        ['code' => 'gb', 'description' => 'United Kingdom'],
        ['code' => 'fr', 'description' => 'France'],
        ['code' => 'de', 'description' => 'Germany'],
    ]
];
